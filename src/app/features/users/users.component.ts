import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/core/services/users.service';
import { IUser } from 'src/app/shared/interfaces/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  users: IUser[];
  displayedColumns: string[] = ['id', 'name', 'username', 'email'];
  
  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
    this.usersService.getUsers().subscribe(data => this.users = data)
  }

  onClick(id: string) {
    this.router.navigate([`/user/${id}`])
  }
}
