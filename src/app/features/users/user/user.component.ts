import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/core/services/users.service';
import { ActivatedRoute } from '@angular/router';
import { IUser } from 'src/app/shared/interfaces/user';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  id: string
  user: IUser

  constructor(private http: UsersService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.http.getUser(this.route.snapshot.params.id).subscribe(data => this.user = data);
  }

  goBack() {
    this.location.back();
  }
}
