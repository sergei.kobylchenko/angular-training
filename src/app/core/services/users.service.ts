import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/app/shared/config';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/shared/interfaces/user';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) { }
  
  getUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(`${API_URL}users`)
  }

  getUser(id: string): Observable<IUser> {
    return this.http.get<IUser>(`${API_URL}users/${id}`)
  }
}